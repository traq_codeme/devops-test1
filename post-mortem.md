# Super, extra, proffesional postmortem

### Uage

* Just look on run.sh as a exmple of usage, and pipline build
* Secrets are in env.sh which in normal condition will be stored in CI/CD for example Jenkins or Gitlab
* user with password name: userwithpassword

## Findings

### App

* Default port in os.getenv is 8080
* Lack of gunicorn in requirements.txt. Most recent version fit to Flask==0.12.2 is Gunicorn==19.7.1 
* readme fix from `api?APP_SECRET=mySecret` to: `api?API_SECRET=mySecret`
* improve main.py from: `logging.error("Wrong secret")` to: `logging.error('Wrong secret: %s', API_SECRET)`
* addedd logger to file config

### Docker

* Two copy? It's adding unecessary layers - fixed
* App run from root - changed to user app
* No apparmor - in the future need to create apparrmor profile /etc/apparmor.d/containers/unit9app and set  apparmor=unit9app
* Need to add port - done by env. var.
* Need to add healthckeck


### Terraform

* create env.sh file with env variables needed by terraform, and usage: source env.sh
* create remote terraform remote s3 state, and dynamodb lock 
* create as a module ec2 instance in default vpc but own security group

### Ansible

* roles creation: part = role
* prepare verions for both Linux ecosystem: redhat and debina with auto detection
* add useres wirh only ssh key auth, and modify /etc/sudoers in redhat version for sudo without password ability
* password encryption to sha512 for /etc/shadow format:  

`ansible all -i localhost, -m debug -a "msg={{ 'somepassword' | password_hash('sha512', 'somesalt') }}"`

* add user with password, and modify /etc/ssh/sshd_config to PasswordAuthentication yes, and password stored in encrypted by ansible vault file

### Tests

* Added  test/locustfile.py 
* In test/results.txt are stored results locust test
* App bottleneck is of course: /api/heavy which is of courcse Fibonacci

### CI/CD

* Makefile is only as a local substitue pipeline, and as a good base for future piplien steps
* Some Jenkisfile added

