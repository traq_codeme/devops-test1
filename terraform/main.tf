module "unit9_demo_ec2_show" {
   
    source      = "./modules/ec2"
    aws_zone    = "us-east-2a"
    app_port    = "8080"
    ssh_port    = "22"
    region      = "us-east-2"
    vpc_id      = "vpc-4b5a8323"
        
}
output "ec2_ip" {
  value = "${module.unit9_demo_ec2_show.ec2_pub_ip}"
}
