resource "aws_key_pair" "ssh_key" {
  key_name = "traq"
  public_key = file("${var.pub_key_path}")
}

resource "aws_instance" "unit9_demo_ec2" {
  ami   = var.ami 
  instance_type = "t2.medium"
  key_name = aws_key_pair.ssh_key.id
  subnet_id = aws_subnet.subnet.id
  vpc_security_group_ids = [aws_security_group.unit9_demo_sg.id]
  associate_public_ip_address = true
  source_dest_check = false
  
  connection {
    # host = self.private_ip
    host = self.public_ip
    user = "ec2-user"
    private_key = file("${var.prv_key_path}")
  }

  tags          = {
      Name        = "unit9_demo_ec2"
  }
}