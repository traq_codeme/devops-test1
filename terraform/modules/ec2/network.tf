#subnet
resource "aws_subnet" "subnet" {
  vpc_id = var.vpc_id
  cidr_block = var.subnet
  availability_zone = var.aws_zone
  tags = {
    Name = "unit9_demo_subnetwork"
  }
}
#security group
resource "aws_security_group" "unit9_demo_sg" {
  vpc_id=var.vpc_id
  name = "unit9_demo_sg"
  description = "Allow incoming HTTP connections & SSH access"

  ingress {
    from_port = var.ext_port
    to_port = var.ext_port
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.ssh_port
    to_port = var.ssh_port
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks =  ["0.0.0.0/0"]
  }
  tags = {
    Name = "unit9_demo_sg"
  }
}
