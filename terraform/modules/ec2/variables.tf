variable "aws_zone" {
  type = string
  description = "AWS availability zone"
  default = "us-east-2a"
}

variable "subnet" {
  type = string
  description = "CIDR for the subnet"
  default = "172.31.2.0/24"
}

variable "app_port" {
  type = string
  description = "port for the app"
  default = "8080"
}

variable "ext_port" {
  type = string
  description = "port for the app"
  default = "80"
}

variable "ssh_port" {
  type = string
  description = "port for the app"
  default = "22"
}

variable "ami" {
  type = string
  description = "Amazon Linux AMI"
  default = "ami-02bcbb802e03574ba"
}

variable "pub_key_path" {
  type = string
  default="~/.ssh/id_rsa.pub"
}

variable "prv_key_path" {
  type = string
  default="~/.ssh/id_rsa"
}

variable "region" {
  type = string
  default="us-east-2"
}

variable "vpc_id" {
  type = string
  default="vpc-4b5a8323"
} 