#!/bin/bash


# import data wich would be in ci/cd env settings plus secrets
source env.sh

# create ec2 in terraform
REMOTEHOST=`make apply | grep ec2_ip | awk '{print $3}'`

# update [update the host system packages]
make update

# # deploy [update the application code and rerun the container],
make deploy

# # install_and_create_env [add users, setup docker]
make install

#run tests
make locust
