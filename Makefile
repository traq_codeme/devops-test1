
apply:
ifndef AWS_SERVICE
	$(error AWS_SERVICE is not set, please: export AWS_SERVICE=someprod)
endif
ifndef AWS_SHARED_CREDENTIALS_FILE
	$(error AWS_SHARED_CREDENTIALS_FILE is not set, please: export AWS_SHARED_CREDENTIALS_FILE=~/.aws/credentials)
endif
ifndef AWS_PROFILE
	$(error AWS_PROFILE is not set, please: export AWS_PROFILE=Some_AWS_Profile)
endif
ifndef AWS_DEFAULT_REGION
	$(error AWS_DEFAULT_REGION is not set, please: export AWS_DEFAULT_REGION=some-region)
endif
ifndef TF_BUCKET
	$(error TF_BUCKET is not set, please: export TF_BUCKET=some-s3-bucket)
endif
ifndef TF_BUCKET_KEY
	$(error TF_BUCKET_KEY is not set, please: export TF_BUCKET_KEY=path/to-key.tfstate)
endif
ifndef DYNAMODB_TABLE
	$(error DYNAMODB_TABLE is not set, please: export DYNAMODB_TABLE=some-dynamodb-table)
endif
	cd terraform && terraform init -backend-config="bucket=$(TF_BUCKET)" -backend-config="key=$(TF_BUCKET_KEY)" -backend-config="region=$(AWS_DEFAULT_REGION)"  -backend-config="profile=$(AWS_PROFILE)" -backend-config="dynamodb_table=$(DYNAMODB_TABLE)"
	cd terraform && terraform plan -out ./plan && terraform apply -auto-approve  -no-color ./plan; rm ./plan


destroy:
ifndef AWS_SERVICE
	$(error AWS_SERVICE is not set, please: export AWS_SERVICE=someprod)
endif
ifndef AWS_SHARED_CREDENTIALS_FILE
	$(error AWS_SHARED_CREDENTIALS_FILE is not set, please: export AWS_SHARED_CREDENTIALS_FILE=~/.aws/credentials)
endif
ifndef AWS_PROFILE
	$(error AWS_PROFILE is not set, please: export AWS_PROFILE=Some_AWS_Profile)
endif
ifndef AWS_DEFAULT_REGION
	$(error AWS_DEFAULT_REGION is not set, please: export AWS_DEFAULT_REGION=some-region)
endif
ifndef TF_BUCKET
	$(error TF_BUCKET is not set, please: export TF_BUCKET=some-s3-bucket)
endif
ifndef TF_BUCKET_KEY
	$(error TF_BUCKET_KEY is not set, please: export TF_BUCKET_KEY=path/to-key.tfstate)
endif
ifndef DYNAMODB_TABLE
	$(error DYNAMODB_TABLE is not set, please: export DYNAMODB_TABLE=some-dynamodb-table)
endif
	cd terraform && terraform destroy -auto-approve


update:
ifndef VAULT_PASS
	$(error VAULT_PASS is not set, please: export VAULT_PASS=some_password)
endif
	echo $(VAULT_PASS) > ~/.vault_pass.txt && ansible-playbook -i ansible/inventory.yml -u ec2-user --vault-password-file ~/.vault_pass.txt  ansible/playbook.yml --private-key=~/.ssh/id_rsa -e "ROLE=update"; rm ~/.vault_pass.txt
deploy:
ifndef VAULT_PASS
	$(error VAULT_PASS is not set, please: export VAULT_PASS=some_password)
endif
	echo $(VAULT_PASS) > ~/.vault_pass.txt && ansible-playbook --vault-password-file ~/.vault_pass.txt -u ec2-user ansible/playbook.yml -i ansible/inventory.yml --private-key=~/.ssh/id_rsa -e "ROLE=deploy"  ; rm ~/.vault_pass.txt

install:
ifndef VAULT_PASS
	$(error VAULT_PASS is not set, please: export VAULT_PASS=some_password)
endif
	echo $(VAULT_PASS) > ~/.vault_pass.txt && ansible-playbook --vault-password-file ~/.vault_pass.txt  -u ec2-user ansible/playbook.yml -i ansible/inventory.yml --private-key=~/.ssh/id_rsa -e "ROLE=install_and_create_env"; rm ~/.vault_pass.txt

ansible: update deploy install

# for local tests
build:
	cd src && sudo docker build -f Dockerfile . --force-rm -t unit9demo

run:
ifndef PORT 
	$(error PORT is not set, please: export PORT=some_app_port)
endif
ifndef API_SECRET
	$(error API_SECRET is not set, please: export API_SECRET=some_password)
endif
	sudo docker run -it --rm -v /Users/michalzuchowski/dev/devops-test1/logs:/usr/src/app/logs -e PORT=$(PORT) -e API_SECRET=$(API_SECRET) -p 80:$(PORT)/tcp unit9demo

runit:
ifndef PORT 
	$(error PORT is not set, please: export PORT=some_app_port)
endif
ifndef API_SECRET
	$(error API_SECRET is not set, please: export API_SECRET=some_password)
endif
	sudo docker run --rm -it -e PORT=$(PORT) -v logs:/usr/src/app/logs/ -e API_SECRET=$(API_SECRET) -p 80:$(PORT)/tcp unit9demo /bin/bash

locust: 
ifndef REMOTEHOST 
	$(error PORT is not set, please: export PORT=some_app_port)
endif
ifndef API_SECRET
	$(error API_SECRET is not set, please: export API_SECRET=some_password)
endif
	locust -f test/locustfile.py -H http://$(REMOTEHOST)


all: