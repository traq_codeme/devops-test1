from locust import HttpLocust, TaskSet, between
import os

API_SECRET = os.getenv('API_SECRET', None)
SEC = "/api?API_SECRET={}".format(API_SECRET)
def index(l):
    l.client.get("/ok")

def error(l):
    l.client.get("/error")

def apisec(l):
    l.client.get("/api?API_SECRET={}".format(API_SECRET))
    # l.client.get("/api?API_SECRET=secret1337")

def apinosec(l):
    l.client.get("/api")

def apiheavy(l):
    l.client.get("/api/heavy")

class UserBehavior(TaskSet):
    tasks = {
        index: 3, 
        error: 1,
        apisec: 1,
        apinosec: 1,
        apiheavy: 2,
    }


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(5.0, 9.0)